use std::sync::Arc;

use bevy::{
    prelude::*,
    render::render_resource::{Extent3d, TextureDimension, TextureFormat},
};
use bevy_egui::{
    egui::{FontFamily, FontId, RichText, Sense, SidePanel},
    EguiContexts,
};

use crate::{
    light::{Light, LightCreator, LightType},
    object::{Object, ObjectCreator, ObjectType},
    render::RenderChannelApplier,
};

#[derive(Default)]
pub struct UiState {
    scene_name: String,
}

#[derive(Default, Resource)]
pub struct OccupiedScreenSpace {
    left: f32,
    right: f32,
}

pub fn camera_system(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

pub fn image_receiver_system(mut commands: Commands, query: Query<&RenderChannelApplier>) {
    if let Ok(RenderChannelApplier(rx)) = query.get_single() {
        let buffer = rx.recv().unwrap();
        commands.spawn(ImageBundle {
            style: Style {
                width: Val::Px(30.0),
                height: Val::Px(30.0),
                ..default()
            },
            image: UiImage::new(Handle::Strong(Arc::new(Image::new(
                Extent3d {
                    width: 1920,
                    height: 1080,
                    depth_or_array_layers: 1,
                },
                TextureDimension::D2,
                buffer,
                TextureFormat::Rgba8Uint,
            )))),
            ..default()
        });
    }
}

pub fn window_system(
    mut commands: Commands,
    mut ui_state: Local<UiState>,
    mut occupied_screen_space: ResMut<OccupiedScreenSpace>,
    mut egui_ctx: EguiContexts,
    light_query: Query<(&Transform, &Light), With<Light>>,
    object_query: Query<(&Transform, &Object), With<Object>>,
) {
    let ctx = egui_ctx.ctx_mut();

    let creator = SidePanel::right("creator_panel")
        .resizable(true)
        .show(ctx, |ui| {
            ui.label(RichText::new("Rusty Zen Creator").font(FontId {
                size: 18.0,
                family: FontFamily::Monospace,
            }));

            ui.horizontal(|ui| {
                ui.label("Scene name: ");
                ui.text_edit_singleline(&mut ui_state.scene_name);
            });

            ui.collapsing("Add Lights", |ui| {
                if ui.button("Point Light").clicked() {
                    commands.spawn((LightCreator(LightType::Point),));
                }

                let _ = ui.button("Area light");
            });

            ui.collapsing("Add Objects", |ui| {
                if ui.button("Line").clicked() {
                    commands.spawn((ObjectCreator(ObjectType::Line),));
                }

                let _ = ui.button("Rectangle");
                let _ = ui.button("Curves");
                let _ = ui.button("Triangle");
            });

            if ui.button("Render!").clicked() {
                let lights = light_query.iter();
                let objects = object_query.iter();

                crate::render::setup(
                    &mut commands,
                    Vec2::new(1920.0, 1080.0),
                    ui_state.scene_name.clone(),
                    lights,
                    objects,
                );
            }

            ui.allocate_rect(ui.available_rect_before_wrap(), Sense::hover());
        });

    occupied_screen_space.right = creator.response.rect.width();
}
