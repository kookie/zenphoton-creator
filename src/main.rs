mod light;
mod object;
mod render;
mod ui;

use bevy::prelude::*;
use bevy_cursor::CursorInfoPlugin;
use bevy_egui::EguiPlugin;

#[derive(Component)]
pub struct RootPosition(pub Vec2);

fn main() {
    App::new()
        .add_plugins((DefaultPlugins, CursorInfoPlugin, EguiPlugin))
        .init_resource::<ui::OccupiedScreenSpace>()
        .add_systems(Startup, (ui::camera_system,))
        .add_systems(
            Update,
            (
                ui::window_system,
                light::move_light_creator,
                object::move_object_creator,
            ),
        )
        .run();
}
