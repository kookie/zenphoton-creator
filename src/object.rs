use crate::RootPosition;
use bevy::{prelude::*, sprite::MaterialMesh2dBundle};
use bevy_cursor::CursorInfo;

#[derive(Component)]
pub struct ObjectCreator(pub ObjectType);

#[derive(Component)]
pub struct Object(pub ObjectType);

#[derive(Clone)]
pub enum ObjectType {
    Line,
    Box,
    Curve,
}

pub fn move_object_creator(
    mut commands: Commands,
    mut t_query: Query<&mut Transform, With<ObjectCreator>>,
    r_query: Query<&RootPosition, With<ObjectCreator>>,
    e_query: Query<(Entity, &ObjectCreator), With<ObjectCreator>>,
    cursor: Res<CursorInfo>,
    buttons: Res<Input<MouseButton>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    match r_query.get_single() {
        Ok(root) => {
            let mut t = t_query.single_mut();
            if let Some(cursor_position) = cursor.position() {
                let diff = root.0 - cursor_position;
                t.scale = Vec3::new(diff.length(), 1.0, 0.0);
                t.rotation = Quat::from_rotation_z(diff.y.atan2(diff.x));

                if buttons.just_pressed(MouseButton::Left) {
                    let (entity, creator) = e_query.single();
                    commands
                        .entity(entity)
                        .remove::<ObjectCreator>()
                        .insert(Object(creator.0.clone()));
                }
            }
        }
        Err(_) => {
            if buttons.just_pressed(MouseButton::Left) {
                if let Ok((entity, _)) = e_query.get_single() {
                    info!("Spawn new quad entity!");
                    let cursor_position = cursor.position().unwrap();

                    commands
                        .entity(entity)
                        .insert(MaterialMesh2dBundle {
                            mesh: meshes
                                .add(shape::Quad::new(Vec2::new(2., 2.)).into())
                                .into(),
                            material: materials.add(ColorMaterial::from(Color::LIME_GREEN)),
                            transform: Transform::default().with_translation(Vec3::new(
                                cursor_position.x,
                                cursor_position.y,
                                0.0,
                            )),
                            ..default()
                        })
                        .insert(RootPosition(cursor_position));
                }
            }
        }
    }
}
