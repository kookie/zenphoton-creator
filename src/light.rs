use crate::RootPosition;
use bevy::{prelude::*, sprite::MaterialMesh2dBundle};
use bevy_cursor::CursorInfo;

#[derive(Component)]
pub struct LightCreator(pub LightType);

#[derive(Component)]
pub struct Light(pub LightType);

#[derive(Clone)]
pub enum LightType {
    Point,
    Area,
}

pub fn move_light_creator(
    mut commands: Commands,
    mut t_query: Query<&mut Transform, With<LightCreator>>,
    r_query: Query<&RootPosition, With<LightCreator>>,
    e_query: Query<(Entity, &LightCreator), With<LightCreator>>,
    cursor: Res<CursorInfo>,
    buttons: Res<Input<MouseButton>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    match r_query.get_single() {
        Ok(root) => {
            let mut t = t_query.single_mut();
            if let Some(cursor_position) = cursor.position() {
                let diff = root.0 - cursor_position;
                t.scale = Vec3::new(diff.length(), diff.length(), 0.0);

                if buttons.just_pressed(MouseButton::Left) {
                    let (entity, creator) = e_query.single();
                    commands
                        .entity(entity)
                        .remove::<LightCreator>()
                        .insert(Light(creator.0.clone()));
                }
            }
        }
        Err(_) => {
            if buttons.just_pressed(MouseButton::Left) {
                if let Ok((entity, _)) = e_query.get_single() {
                    info!("Spawn new circle entity!");
                    let cursor_position = cursor.position().unwrap();

                    commands
                        .entity(entity)
                        .insert(MaterialMesh2dBundle {
                            mesh: meshes.add(shape::Circle::new(1.0).into()).into(),
                            material: materials.add(ColorMaterial::from(Color::YELLOW)),
                            transform: Transform::default().with_translation(Vec3::new(
                                cursor_position.x,
                                cursor_position.y,
                                0.0,
                            )),
                            ..default()
                        })
                        .insert(RootPosition(cursor_position));
                }
            }
        }
    }
}
