use crate::{
    light::{Light, LightType},
    object::{Object, ObjectType},
};
use bevy::prelude::*;
use crossbeam_channel::{bounded, Receiver};
use rustic_zen::{
    material::hqz_legacy,
    sampler::Sampler,
    scene::{Light as ZenLight, RenderConstraint, Scene, Segment},
};
use std::{ops::Add, thread};

#[derive(Component)]
pub struct RenderChannelApplier(pub Receiver<Vec<u8>>);

pub fn setup<'a>(
    commands: &mut Commands,
    screen_size: Vec2,
    scene_name: String,
    lights: impl Iterator<Item = (&'a Transform, &'a Light)>,
    objects: impl Iterator<Item = (&'a Transform, &'a Object)>,
) {
    let m = hqz_legacy(0.3, 0.3, 0.3);
    let mut s = Scene::new(screen_size.x as usize, screen_size.y as usize);

    for (transform, object) in objects {
        match object {
            Object(ObjectType::Line) => {
                let root = Vec2::new(transform.translation.x, transform.translation.y);
                let other_end =
                    root.add(Vec2::new(transform.scale.x / 2.0, transform.scale.y / 2.0));

                let o = Segment::line_from_points(
                    (root.x as f64, root.y as f64),
                    (other_end.x as f64, other_end.y as f64),
                    m.clone(),
                );

                s.add_object(o);
            }
            _ => todo!(),
        }
    }

    for (transform, light) in lights {
        match light {
            Light(LightType::Point) => {
                let l = ZenLight::new(
                    (
                        // location
                        transform.translation.x as f64,
                        transform.translation.y as f64,
                    ),
                    transform.scale.x as f64,        // power
                    (0.0, transform.scale.x as f64), // polar_distance
                    (0.0, transform.scale.x as f64), // polar_angle
                    (360.0, 0.0),                    // ray_angle
                    Sampler::new_blackbody(3800.0),  // wavelength
                );

                s.add_light(l);
            }
            _ => todo!(),
        }
    }

    let (tx, rx) = bounded::<Vec<u8>>(1);

    thread::spawn(move || {
        let (num_rays, image) = s.render(RenderConstraint::Count(1_000_000), 16);
        let vec = image.to_rgb8(num_rays, 0.4, 1.2);
        let vec: Vec<u8> = vec
            .windows(3)
            .map(|w| {
                let mut v = w.to_vec();
                v.push(0);
                v
            })
            .flatten()
            .collect();
        tx.send(vec);
    });

    commands.spawn((RenderChannelApplier(rx),));
}
